###############################################################################
#
# Rakefile for building the savapi-based av components for macOS
#
# Usage 'rake [rev=<rev>] to build all.
#
# The 'rev' parameter sets the third digit of the version string.
#
# You have options to build individual components. Use 'rake -T' for a list of 
# all available build tasks.
#
###############################################################################

require 'rake'

$requiredXCodeVersion = "9.2"

class FailedBuildException < Exception;end

task :default => [:make_projects]

###############################################################################
# Top level tasks
###############################################################################

desc "make the IDE projects. This is a handy task for getting ready to do IDE-based development/debugging"
task :make_projects => [:build_projucer, "app:jucer"] do
  puts "make_projects is complete"
end

desc "build Projucer"
task :build_projucer do
  puts "\nBuilding Projucer"
  testXcodeVersion
  build_result = %x{xcodebuild -project ~/JUCE/extras/Projucer/Builds/MacOSX/Projucer.xcodeproj -configuration Release}
  if !build_result.match(/build succeeded/i)
    puts "build failed with:"
    puts build_result.lines.grep(/error/i)
    throw FailedBuildException.new
  end
end

###############################################################################
# RecordPlayBackDemo app tasks
###############################################################################
namespace :app do
    
   desc "make the IDE projects for the demo application"
  task :jucer do
    puts "\nMaking the projects for the demo application."

    rm_rf "Builds"
sh "~/JUCE/extras/Projucer/Builds/MacOSX/build/Release/Projucer.app/Contents/MacOS/Projucer --resave RecordPlaybackDemo.jucer"


  end
   
end

###############################################################################
# utility functions
###############################################################################

module Rake
  class Task
    def execute_with_timestamps(*args)
      start = Time.now
      execute_without_timestamps(*args)
      execution_time_in_seconds = Time.now - start
      printf("** %s took %.1f seconds\n", name, execution_time_in_seconds)
    end
    alias :execute_without_timestamps :execute
    alias :execute :execute_with_timestamps 
  end
end

class FailedXcodeTestException < Exception;end
def testXcodeVersion
  version = `xcodebuild -version`
  if !version.match(/Xcode #{$requiredXCodeVersion}/)
    puts "Expected Xcode version to be #{$requiredXCodeVersion}"
    throw FailedXcodeTestException.new
  end 
end

# README #

Last updated March 25th, 2018

J. Koutavas

## Introduction

Welcome to the repository for the OpPop JUCE-based RecordPlayBackDemo app. 

The app will build and run on both macOS and iOS (with Android support to follow)

There is a rakefile in the root directory that will generate the macOS and iOS Xcode projects for the app. (With Android Studio project to follow.)

## building

For macOS and iOS builds, you'll need Sierra or High Sierra, with Xcode 9.2 installed.

Download the .zip of JUCE v.5.3.0 from https://github.com/WeAreROLI/JUCE/releases/tag/5.3.0

Unpack the zip and place it at ~/JUCE

From Terminal, do this: 
````
$ cd <clone-root>
$ rake
````

This will create a <clone-root>/build directory which contains 'MacOSX' and 'iOS'. The xcode projects are found in these sub-directories.


/*
  ==============================================================================

    AudioDemo.h
    Created: 21 Mar 2018 6:33:11pm
    Author:  Jay Koutavas

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"

extern ScopedPointer<AudioDeviceManager> sharedAudioDeviceManager;

AudioDeviceManager& getSharedAudioDeviceManager (int numInputChannels = -1, int numOutputChannels = -1);


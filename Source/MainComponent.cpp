/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"
#include "AudioPlaybackDemo.h"
#include "AudioRecordingDemo.h"
#include "SettingsContent.h"

//==============================================================================
MainComponent::MainComponent()
{
    tabsComponent = new TabbedComponent(TabbedButtonBar::Orientation::TabsAtTop);
    addAndMakeVisible(tabsComponent);
    
    // it's important to initialize the SettingsContent prior to the recording and playback demos. Otherwise getSharedAudioDeviceManager() doesn't get initialized right
    SettingsContent *settings = new SettingsContent(*this);
    
    AudioRecordingDemo *recording = new AudioRecordingDemo;
    AudioPlaybackDemo *playback = new AudioPlaybackDemo;
    
    tabsComponent->addTab(translate("Record"), Colours::transparentBlack, recording, true);
    tabsComponent->addTab(translate("Playback"), Colours::transparentBlack, playback, true);
    tabsComponent->addTab(translate("Settings"), Colours::transparentBlack, settings, true);
    tabsComponent->setTabBarDepth(40);
    tabsComponent->getTabbedButtonBar().setColour(TabbedButtonBar::frontTextColourId, Colours::cornflowerblue);
    
    setOpaque (true);
 
#if JUCE_MAC
    setSize (800,800);
#else
    setSize (320, 560);
#endif

}

MainComponent::~MainComponent()
{
    tabsComponent = nullptr;
    sharedAudioDeviceManager.reset();
}

void MainComponent::paint (Graphics& g)
{
    g.fillAll (findColour (ResizableWindow::backgroundColourId));
}

void MainComponent::resized()
{
    auto bounds = getLocalBounds();
    tabsComponent->setBounds(bounds);
}

void MainComponent::parentHierarchyChanged()
{
    auto* newPeer = getPeer();

    if (peer != newPeer)
    {
        peer = newPeer;

        auto previousRenderingEngine = renderingEngines[currentRenderingEngineIdx];

        renderingEngines.clear();
        if (peer != nullptr)
            renderingEngines = peer->getAvailableRenderingEngines();

        renderingEngines.add ("OpenGL Renderer");

        currentRenderingEngineIdx = renderingEngines.indexOf (previousRenderingEngine);

        if (currentRenderingEngineIdx < 0)
        {
           #if JUCE_ANDROID
            currentRenderingEngineIdx = (renderingEngines.size() - 1);
           #else
            currentRenderingEngineIdx = peer->getCurrentRenderingEngine();
           #endif
        }

        updateRenderingEngine (currentRenderingEngineIdx);
    }
}

void MainComponent::updateRenderingEngine (int renderingEngineIndex)
{
    if (renderingEngineIndex == (renderingEngines.size() - 1))
    {
        openGLContext.attachTo (*getTopLevelComponent());
    }
    else
    {
        openGLContext.detach();
        peer->setCurrentRenderingEngine (renderingEngineIndex);
    }

    currentRenderingEngineIdx = renderingEngineIndex;
}


void MainComponent::setRenderingEngine (int renderingEngineIndex)
{
    if (renderingEngineIndex != currentRenderingEngineIdx)
        updateRenderingEngine (renderingEngineIndex);
}
